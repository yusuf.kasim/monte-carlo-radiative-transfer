# -*- coding: utf-8 -*-
"""
Created May 2020

@author: Yusuf Kasim
"""

import matplotlib.pyplot as plt
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

data = np.genfromtxt('tele.txt',delimiter=' ')

x=data[:,0]
y=data[:,1]
z=data[:,2]

x=np.unique(x)
y=np.unique(y)
X,Y = np.meshgrid(x,y)

Z=z.reshape(len(y),len(x))

plt.pcolormesh(X,Y,Z)

plt.savefig("results.png",format='png',dpi=900)

plt.clf()

data2 = np.genfromtxt('res1stph.txt',delimiter=' ')
data3 = np.genfromtxt('res2ndph.txt',delimiter=' ')


x2=data2[:,0]
y2=data2[:,1]
z2=data2[:,2]

x3=data3[:,0]
y3=data3[:,1]
z3=data3[:,2]

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.plot3D(x2,y2,z2,'red')
ax.plot3D(x3,y3,z3,'blue')


plt.savefig("traject.png",format='png',dpi=900)