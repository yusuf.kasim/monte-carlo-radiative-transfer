#!/bin/tcsh -f 
CC=g++ ;

INC= -I ./share 
CFLAGS= -O6 -Wall --ansi -std=c++14
TARGET= MC.exe
SOURCES= main.cpp photon.cpp model.cpp 
OBJECTS= main.o photon.o model.o 
code:
	g++ -c $(CFLAGS) $(SOURCES) $(INC)
	@echo "Objects linked = $(OBJECTS)"
	g++ -o $(TARGET) $(OBJECTS) 

