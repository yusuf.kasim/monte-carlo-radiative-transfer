#ifndef MODEL_H_INCLUDED
#define MODEL_H_INCLUDED
#include <vector>
#include "photon.h"

using namespace std;


class model
{
private:
    int N; //number of photons in each source
    vector <photon>Packet; //a vector holding all the photons
    double absorb; //absorption rate
    double (*mat_dist)(double,double,double); //the function that defines the distribution of matter
    vector<vector<int>> hits;//a telescope defined as a 2d matrix


public:
    model(int,double (*)(double,double,double),double);
    void add_source(double,double,double,double,double); // adding a new source which in turn means increasing the photon packet by N photons
    void propagate(int); // a function propagating all the photons
    void Prt_telescope(); //printing the txt file used for graphing
};



#endif // MODEL_H_INCLUDED
