#include <iostream>
#undef __STRICT_ANSI__
#include <cmath>
#include <math.h>
#include <vector>
#include <random>
#include "photon.h"

default_random_engine gen;
uniform_real_distribution<double> dist(0,1);


//all the functions here are described in the .h file

using namespace std;


photon::photon(double x0, double y0, double z0,double l)
{
    x = x0;
    y= y0;
    z= z0;
    k = sqrt(l);
    vx =0;
    vy =0;
    vz =0;
    ab_st = 0;
    hit_st = 0;
}


vector<double> photon::GetCoord() const
{
    vector<double> coord(3);
    coord[0] =x;
    coord[1] =y;
    coord[2] =z;
    return coord;
}

vector<double> photon::GetV() const
{
    vector<double> V(3);
    V[0] =vx;
    V[1] =vy;
    V[2] =vz;
    return V;
}


void photon::initialize_ph(double x0, double y0, double z0, double l)
{
    x = x0;
    y= y0;
    z= z0;
    k = sqrt(l);
    vx =0;
    vy =0;
    vz =0;
    ab_st = 0;
    hit_st = 0;
}

void photon::shf(){
    double phi = 2*M_PI*dist(gen);
    double theta = M_PI*dist(gen);
    vx = sin(theta)*cos(phi);
    vy = sin(theta)*sin(phi);
    vz = cos(theta);
}

void photon::mov(double L)
{
    x += L*vx;
    y += L*vy;
    z += L*vz;
}

void photon::absrp(){
    ab_st = 1;
}

double photon::calcrosssec(){
    return k;
}

void photon::hit(){
    hit_st = 1;
}
