#include <iostream>
#include <cmath>
#include <math.h>
#include <fstream>
#include <random>
#include <stdlib.h>
#include "photon.h"
#include "model.h"


//////////////////////////////////////////////////////////////////////////////////////
//             RADIATIVE MONTE CARLO
//             CODE BY YUSUF KASIM - MAY 2020
//             M1 PROGRAMMING PROJECT
//////////////////////////////////////////////////////////////////////////////////////
//                     USER INSTRUCTIONS
// This code is made to simulate radiative transfer using Monte Carlo methods
// You need to define a function taking x,y,z coordinate and give the density of the medium
// Then you define an object called model with the argument number of photons, pointer of the density and probability of absorption
// You need then to use the function add source to add a photon source the arguments are x,y,z positions, minimum and maximum wavelength
// You should make sure to put the object between x,y [-500,500] other wise the telescope function should be changed
// Finally you should use the function propagate with the number of desired reflection of each photon
///////////////////////////////////////////////////////////////////////////////////////




using namespace std;

double density(double x, double y, double z){ //the density function
    double r0 = 10;
    if (sqrt(pow(x,2)+pow(y,2)+pow(z,2)) < 1  ){return r0;}
    else {return r0 * (1.0/sqrt(pow(x,2)+pow(y,2)+pow(z,2)));}

}



int main()
{


    model test(2000, &density,0); //declaring the model
    test.add_source(250,250,0,1,1); // adding the sources
    test.add_source(250,-250,0,10,10);
    test.add_source(-250,250,0,100,100);
    test.add_source(-250,-250,0,1000,1000);
    test.propagate(200);

    system("python plot.py"); //plotting using an external python code


    return 0;
}
