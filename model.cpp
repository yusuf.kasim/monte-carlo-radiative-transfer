#include <iostream>
#include <vector>
#include <random>
#include <fstream>
#include "photon.h"
#include "model.h"


using namespace std;

default_random_engine gen2;
uniform_real_distribution<double> dist2(0,1);


//the functions are described in the .h file

model::model(int N0, double(*a)(double,double,double),double abr)
{
    N = N0;
    mat_dist = a;
    absorb = abr;
    vector<vector<int>> s(100,vector<int>(100,0));
    hits = s;

}


void model::add_source(double x0,double y0, double z0,double maxl, double minl)
{
    double diffl = maxl-minl;
    for (int i=0;i<N;i++){
        double L = minl + (dist2(gen2));
        photon a(x0,y0,z0,L);
        Packet.push_back(a);
    }
}


void model::propagate(int nd)
{
    ofstream res("res1stph.txt");
    ofstream res2("res2ndph.txt");


    double n,tau,dl,l,sig;



    for(int i=0; i<Packet.size();i++)
    {
        for (int j=0;j<nd;j++)
        {
            Packet[i].shf();
            vector<double> ip = Packet[i].GetCoord();
            vector<double> iv = Packet[i].GetV();


            if(i==0) {res << ip[0] << " " << ip[1] << " " << ip[2] << " " << endl;} //printing the first two photons for the trajectory image
            if(i==1) {res2 << ip[0] << " " << ip[1] << " " << ip[2] << " " << endl;}


            if(Packet[i].GetSt()==0){
                l=0;
                n = mat_dist(ip[0],ip[1],ip[2]);
                tau = -log(1-dist2(gen2));
                sig = Packet[i].calcrosssec();
                dl = tau/(n*sig*100);
                double ts =0;
                while (ts<tau)
                {
                    for(int k =0;k<3;k++){ip[k]+= dl*iv[k];} //doing the integral of the optical length
                    ts += (mat_dist(ip[0],ip[1],ip[2]))*dl*0.5;
                    l += dl;
                    if(Packet[i].GetHit()==0 && ip[2]>=50  && ip[2] <= 51 && ip[1] > -490 && ip[1] < 500 && ip[0] > -490 && ip[0] < 500 ){ //checking if the photon hits the telescope
                        hits[round(ip[0]*0.1)+49][round(ip[1]*0.1)+49] += 1;
                        Packet[i].hit();
                    }
                }
                Packet[i].mov(l);
                if(dist2(gen2)<absorb){Packet[i].absrp();}
            }

        }
    }


    Prt_telescope();
}

void model::Prt_telescope()
{
    ofstream tele("tele.txt");
    for(int i =0;i<100;i++){
        for (int j=0;j<100;j++){
            tele << i-49 << " " << j-49 << " " << hits[i][j] << endl ;
        }
    }
}
