#ifndef PHOTON_H_INCLUDED
#define PHOTON_H_INCLUDED
#include <vector>

using namespace std;


class photon
{
private:
    double x; //coordinates
    double y;
    double z;
    double vx; //speed
    double vy;
    double vz;
    double k; //to calculate the cross section
    int ab_st; //absorption state
    int hit_st; //detection by the telescope

public:
    photon(double,double,double,double); //starting position
    photon();
    void initialize_ph(double,double,double,double); //initializing function if empty photon is created
    vector<double> GetCoord() const; //function that give back the coordinates
    vector<double> GetV() const; // function that gives back the velocities
    double calcrosssec(); // function that give back the cross section
    int GetSt() const {return ab_st;}; // if the photon is absorbed or not
    int GetHit() const {return hit_st;}; // if the photon is detected by the telescope
    void shf(); // shuffle the velocities and define new ones
    void mov(double); // move the photon by a certain length according the the internal velocity
    void absrp(); // change the absorption state of the photon
    void hit(); // change the telescope detection state of the photon
};


#endif // PHOTON_H_INCLUDED
